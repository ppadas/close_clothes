# Problem A. Key Word in Context (KWIC)
##### _Решение задачи методом Implicit Invocation (Event-Driven)_

#### [Статья про KWIC-индексацию](https://en.wikipedia.org/wiki/Key_Word_in_Context "Статья на википедии")    

#### Описание решения

Для решения этой задачи я выделил две сущности, Analyzer и KWICIndexEntries. Analyzer анализирует исходное предложение, выделяет из него слова и игнорирует стоп-слова. Как только он нашел слово, которое является ключевым, он уведомляет об этом объект класса KWICIndexEntries, который в свою очередь генерирует перестановку исходного предложения, в которой данное слово стоит на первом месте, и сохраняет это предложение в индексе.   

#### Требования   
* ##### Python 3.9+
* ##### VirtualEnv

#### Установка
 ```
 virtualenv ./
 source ./bin/activate # Активация виртуальной среды
 python -m ensurepip --upgrade # Установка pip
 pip install -r requirements.txt # Установка зависимостей
 ```
 
#### Использование
```
usage: kwic.py [-h] sentence
positional arguments:
  sentence    The sentence for which indexing will be performed
optional arguments:
  -h, --help  show this help message and exit
```

#### Пример запуска
```
Input:
    python src/kwic.py 'Essentials of Business communication Russia in Machine Learning 129'
Output:
    =======================================================================================
    KWIC-Index for "Essentials of Business communication Russia in Machine Learning 129"
    =======================================================================================
    1. 129 \ Essentials of Business communication Russia in Machine Learning
    2. Business communication Russia \ Machine Learning 129 \ Essentials
    3. communication Russia \ Machine Learning 129 \ Essentials of Business
    4. Essentials \ Business communication Russia in Machine Learning 129
    5. Learning 129 \ Essentials of Business communication Russia in Machine
    6. Machine Learning 129 \ Essentials of Business communication Russia
    7. Russia \ Machine Learning 129 \ Essentials of Business communication
    =======================================================================================
```
