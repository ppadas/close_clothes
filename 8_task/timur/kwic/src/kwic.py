"""Implementation of KWIC-indexing using Implicit invocation (event-driven) method."""
from argparse import ArgumentParser
from asyncio import get_event_loop, gather
from copy import deepcopy
from typing_extensions import override
import nltk


class IObserver:
    """Interface for observer entities"""
    def handle_event(self, keyword_ind: int) -> None:
        """Add keyword entry to KWIC-index"""

class Analyzer:
    """Select keywords from sentence with stopwords"""
    def __init__(self, sentence: str, stopwords: set[str]) -> None:
        self.__sentence = sentence
        self.__stopwords = stopwords
        self.__observers = []

    def analyze(self) -> None:
        """Add keywords from sentence to KWIC-index"""
        for ind, keyword in enumerate(self.__sentence.split(" ")):
            if keyword in self.__stopwords:
                continue

            self.__notify(ind)

    def __notify(self, keyword_ind: int) -> None:
        for observer in self.__observers:
            observer.handle_event(keyword_ind)

    def subscribe(self, observer: IObserver) -> None:
        """Add observer for Analyzer"""
        self.__observers.append(observer)

class KWICIndexEntries(IObserver):
    """Class for collecting generated entries of KWIC-index"""

    def __init__(self, sentence: str, stopwords: set[str]) -> None:
        self.__sentence = sentence
        self.__stopwords = stopwords
        self.__index = []
        self.__tasks = []
        self.__sep = ' \ '

    @override
    def handle_event(self, keyword_ind: int) -> None:
        self.__tasks.append(self.__create_entry(keyword_ind))

    async def __create_entry(self, keyword_ind: int) -> None:
        words = self.__sentence.split(" ")

        start_ind = keyword_ind
        end_ind = len(words)

        for j in range(start_ind + 1, len(words)):
            if words[j] in self.__stopwords:
                end_ind = j
                break

        keyword_part = " ".join(words[start_ind:end_ind])

        before_part_end = start_ind - 1
        while before_part_end > 0 and words[before_part_end] in self.__stopwords:
            before_part_end -= 1

        before_part = " ".join(words[0:before_part_end + 1])

        after_part_start = end_ind
        while after_part_start < len(words) and words[after_part_start] in self.__stopwords:
            after_part_start += 1

        after_part = " ".join(words[after_part_start:])

        result = keyword_part
        if after_part != "":
            result += self.__sep + after_part

        if before_part != "":
            result += self.__sep + before_part

        self.__index.append(result)

    async def get_index(self):
        """Get sorted KWIC-index"""
        await gather(*self.__tasks)
        results = deepcopy(self.__index)
        results.sort(key=lambda x: x.split(" ")[0].lower())

        return results


def load_eng_stopwords() -> set[str]:
    """Get english stopwords from nltk library"""
    try:
        nltk.data.find('corpora/stopwords')
    except LookupError:
        nltk.download('stopwords')

    return set(nltk.corpus.stopwords.words('english'))


async def main():
    """Main function"""

    parser = ArgumentParser(
        description='Perform KWIC-indexing on given sentence.'
    )

    parser.add_argument('sentence', metavar='sentence', type=str,
                        help='The sentence for which indexing will be performed')

    sentence = parser.parse_args().sentence
    stopwords = load_eng_stopwords()

    kwic_index = KWICIndexEntries(sentence, stopwords)
    analyzer = Analyzer(sentence, stopwords)

    analyzer.subscribe(kwic_index)

    analyzer.analyze()

    index = await kwic_index.get_index()
    print("=" * (len(sentence) + 20))
    print(f"KWIC-Index for \"{sentence}\"")
    print("=" * (len(sentence) + 20))
    for i, entry in enumerate(index):
        print(f"{i + 1}. {entry}")
    print("=" * (len(sentence) + 20))

if __name__ == "__main__":
    loop = get_event_loop()
    loop.run_until_complete(main())
