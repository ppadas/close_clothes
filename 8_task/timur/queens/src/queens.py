"""Program which solves Eight Queen problems using Gradual Refinement"""


class QueenPlacements:
    """Generate all possible placements of 8 queens on chess board"""

    def __init__(self) -> None:
        self.__grid = [[0] * 8 for _ in range(8)]
        self.__place_queens()
        self.__initial_arrangement = True

    def __place_queens(self) -> None:
        for row in range(8):
            self.__place_queen_initial(row)

    def __place_queen_initial(self, ind: int) -> None:
        self.__grid[ind] = [0] * 8
        self.__grid[ind][0] = 1

    def __iter__(self):
        return self

    def __next__(self) -> list[list[int]]:
        if self.__initial_arrangement:
            self.__initial_arrangement = False
            return self.__grid

        row = 7

        while row >= 0 and not self.__move_queen_in_row(row):
            self.__place_queen_initial(row)
            row -= 1

        if row == -1:
            raise StopIteration

        return self.__grid

    def __get_queen_placement(self, row: int) -> int:
        try:
            return self.__grid[row].index(1)
        except ValueError:
            return -1

    def __move_queen_in_row(self, row: int) -> bool:
        col = self.__get_queen_placement(row)

        if col < 7:
            self.__remove_queen(row, col)
            col += 1
            self.__place_queen(row, col)
            return True
        else:
            return False

    def __place_queen(self, row: int, col: int):
        self.__grid[row][col] = 1

    def __remove_queen(self, row: int, col: int) -> None:
        self.__grid[row][col] = 0

    def is_valid_placement(self) -> bool:
        """Check if queens are placed correctly"""
        for row in range(8):
            for another_row in range(row + 1, 8):
                if self.__check_queens_hit(row, another_row):
                    return False

        return True

    def show_placement(self) -> None:
        """Print current placement"""
        print(self.__grid)

    def __check_queens_hit(self, row: int, another_row: int) -> bool:
        return self.__vertical_hit(row, another_row) or self.__diagonal_hit(row, another_row)

    def __vertical_hit(self, row: int, another_row: int) -> bool:
        col1 = self.__get_queen_placement(row)
        col2 = self.__get_queen_placement(another_row)

        return col1 == col2

    def __diagonal_hit(self, row: int, another_row: int) -> bool:
        col1 = self.__get_queen_placement(row)
        col2 = self.__get_queen_placement(another_row)

        return abs(row - another_row) == abs(col1 - col2)


def count_valid_arrangements() -> int:
    """Count valid arrangements of 8 queens on chess board"""
    placements = QueenPlacements()

    counter = 0
    for _ in placements:
        if placements.is_valid_placement():
            placements.show_placement()
            counter += 1

    return counter


def main():
    """Main function"""
    print(f"Solutions count: {count_valid_arrangements()}")


if __name__ == "__main__":
    main()
