from collections import deque
import copy

class queen:
    def checkCanEatAnyOne(self, table, position):
        directions = [[0, 1], [1, 1], [1, 0], [1, -1], [0, -1], [-1, -1], [-1, 0], [-1, 1]]
        step = 1
        for direction in directions:
            step = 0
            while 0 < position[0] + direction[0] * step < table.size()[0] and \
                position[1] + direction[1] * step < table.size()[1]:
                if table[position[0] + direction[0] * step][position[1] + direction[1] * step] is not None:
                    return True
                step += 1
        return False
    
    def checkCanEatIt(self, my_position, other_position):
        if my_position[0] == other_position[0] or my_position[1] == other_position[1]:
            return True
        if abs(my_position[0] - other_position[0])  == abs(my_position[1] - other_position[1]):
            return True
        return False

    def getLable(self):
        return "Q"

class Table:
    def __init__(self, size_x, size_y):
        self.table = [[None] * size_y for x in range(size_x)]


    def trySet(self, figure, x, y):
        if self.table[x][y] is not None:
            return False
        self.table[x][y] = figure
        for x_iter in range(len(self.table)):
            for y_iter in range(len(self.table[0])):
                if (x != x_iter or y != y_iter) and self.table[x_iter][y_iter] is not None and \
                    (self.table[x_iter][y_iter].checkCanEatIt([x_iter, y_iter], [x, y]) or \
                        self.table[x][y].checkCanEatIt([x, y], [x_iter, y_iter])):
                    self.table[x][y] = None
                    return False
        return True
    
    def printTable(self):
        for x_iter in range(len(self.table)):
            for y_iter in range(len(self.table[0])):
                if self.table[x_iter][y_iter] is not None:
                    print(self.table[x_iter][y_iter].getLable(), end=' ')
                else:
                    print("-", end=' ')
            print("")

    def removeFigure(self, x, y):
        self.table[x][y] = None

class PositionGenerator:
    def __init__(self, size_x, size_y, queen_counter):
        self.size_x = size_x
        self.size_y = size_y
        self.queen_counter = queen_counter

    def generateNew(self):
        counter = 0
        table = Table(size_x, size_y)

        stack = []
        stack.append([table, [0, 0], 0])
        while len(stack) != 0:
            state = stack.pop() # [table, current_pos, queen_counter]
            [table, current_pos, queens] = state
            if queens == queen_counter:
                yield table
                continue

            if current_pos[0] >= size_x or current_pos[1] >= size_y:
                continue

            next_pos = copy.copy(current_pos)
            next_pos[0] += 1
            if next_pos[0] >= size_x:
                next_pos[0] = 0
                next_pos[1] += 1
            table_with_insert = copy.deepcopy(table)
            table_without_insert = copy.deepcopy(table)

            stack.append([table_without_insert, next_pos, queens])
            if table_with_insert.trySet(queen(), current_pos[0], current_pos[1]):
                stack.append([table_with_insert, next_pos, queens + 1])



queen_counter = 8
size_x = 8
size_y = 8

generator = PositionGenerator(size_x, size_y, queen_counter)
gen_result = generator.generateNew()

a = 0
for table in gen_result:
    table.printTable()
    next_signal = input('To continue press +: ')
    if next_signal != '+':
        break
    a += 1

print("Exit")

