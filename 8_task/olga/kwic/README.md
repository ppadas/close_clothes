# Problem A. Key Word in Context (KWIC)
##### _Решение задачи методом Pipes-and-filters_

#### [Статья про KWIC-индексацию](https://en.wikipedia.org/wiki/Key_Word_in_Context "Статья на википедии")    

#### Описание решения

Я разделила задачу на несколько этапов: выделение значимых слов, выделение костекста, сортировка всех фраз. Под каждую задачу была написана мини программа: not_stop.py, define.py, save.py соответственно. Процессы соединены pipe-ами.

#### Требования   
* ##### Python 3.9+

#### Пример запуска
```
На вход -- строка + длина контекста до и после
Input:
    python3 kwic.py "KWIC is anacronym for Key Word in Context" 2
Output:
    anacronym key word /  kwic
    context /  word key
    key word context /  anacronym kwic
    kwic anacronym key / 
    word context /  key anacronym
```
