from multiprocessing import Process, Pipe
from subprocess import Popen, PIPE
import os, sys

input_str = sys.argv[1]
word_offset = sys.argv[2]

p_define_not_stop_words = Popen(['python3', 'not_stop.py', input_str], stdout=PIPE)
p_make_definition = Popen(['python3', 'define.py', input_str, word_offset], \
    stdin=p_define_not_stop_words.stdout, stdout=PIPE)
p_update_the_list = Popen(['python3', 'save.py'], stdin=p_make_definition.stdout)

p_define_not_stop_words.stdout.close()
p_update_the_list.wait()