import sys
from nltk.corpus import stopwords

data = sys.argv[1]
data = data.split()
for word_iter in range(len(data)):
    if data[word_iter] not in stopwords.words('english'):
        print(word_iter)

print("Exit")
