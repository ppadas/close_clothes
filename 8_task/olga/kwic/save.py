import bisect 

data = []

while True:
    word_def = input()
    if word_def == "Exit":
        break
    else:
        bisect.insort(data, word_def)

for word_def in data:
    print(word_def)
