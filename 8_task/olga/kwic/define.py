import sys
from nltk.corpus import stopwords

data = sys.argv[1]
data = data.lower().split()

offset = int(sys.argv[2])

while True:
    word_index = input()
    if word_index == "Exit":
        break
    else:
        word_index = int(word_index)
        answer = data[word_index]
        right_counter = 0
        current_considered_index = word_index + 1
        while right_counter != offset:
            if current_considered_index >= len(data):
                break
            if data[current_considered_index] not in stopwords.words('english'):
                right_counter += 1
                answer = answer + " " + data[current_considered_index]
            current_considered_index += 1
        answer = answer + " / "
        
        left_counter = 0
        current_considered_index = word_index - 1
        while left_counter != offset:
            if current_considered_index < 0:
                break
            if data[current_considered_index] not in stopwords.words('english'):
                left_counter += 1
                answer = answer + " " + data[current_considered_index]
            current_considered_index -= 1
        
        print(answer)




print("Exit")
        