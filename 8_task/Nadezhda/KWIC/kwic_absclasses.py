class Keywords():
    def __init__(self, phrase):
        self.phrase = phrase
        self.stop_words = []
        with open('stopwords.txt') as f:
            for word in f:
                self.stop_words.append(word[: -1])

        self.keywords = self.findKeywords(phrase)

    def findKeywords(self, phrase):
        words = phrase.split()
        indexes = []
        for i in range(len(words)):
            if words[i] in self.stop_words:
                indexes.append(-1)
            else:
                indexes.append(i)
        return indexes

    def getKeywords(self):
        return self.keywords


class SortedKeywords():
    def __init__(self, keywords):
        self.kw = keywords
        self.sorted_keywords = []

    def sort(self):
        words = self.kw.phrase.split()
        sorted_tmp = []
        for w, kw in zip(words, self.kw.getKeywords()):
            if kw != -1:
                sorted_tmp.append((w.lower(), w, kw))
        sorted_tmp.sort()
        for word in sorted_tmp:
            self.sorted_keywords.append((word[1], word[2]))

    def getSortedKeywords(self):
        return self.sorted_keywords


class Context():
    def __init__(self, phrase, indexes, context_len):
        self.words = phrase.split()
        self.context = []
        for ind in indexes:
            self.context.append(' '.join(self.words[max(0, ind[1] - context_len):
                                                    min(ind[1] + context_len, len(self.words))]))

    def getContext(self):
        return self.context


if __name__ == "__main__":
    phrase = input()
    kw = Keywords(phrase)
    sorted_kw = SortedKeywords(kw)
    sorted_kw.sort()
    skw = sorted_kw.getSortedKeywords()
    cntx = Context(phrase, sorted_kw.getSortedKeywords(), 3).getContext()
    for i in range(len(cntx)):
        print(skw[i][0], ': ', cntx[i])
