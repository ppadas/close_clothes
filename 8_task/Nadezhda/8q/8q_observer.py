from abc import ABC, abstractmethod


class AbstractSolver(ABC):
    @abstractmethod
    def addQueen(queen):
        pass

    @abstractmethod
    def removeQueen(number):
        pass

    @abstractmethod
    def notify():
        pass


class Solver(AbstractSolver):
    def __init__(self, answer_storage):
        self.answer_storage = answer_storage
        self.queens = []
        self.positions = []
        self.diag1 = []
        self.diag2 = []
        self.size = 8

    def addQueen(self, queen):
        self.queens.append(queen)

    def removeQueen(self, number):
        self.queens.pop(number)

    def notify(self):
        for i in range(len(self.queens)):
            self.queens[i].update(self.positions[i])
        self.answer_storage.update()

    def getSolution(self, row):
        if row < self.size:
            for clmn in range(self.size):
                if clmn not in self.positions and \
                        row + clmn not in self.diag1 and \
                        row - clmn not in self.diag2:
                    self.positions.append(clmn)
                    self.diag1.append(row + clmn)
                    self.diag2.append(row - clmn)
                    yield from self.getSolution(row + 1)
                    self.positions.pop()
                    self.diag1.pop()
                    self.diag2.pop()
        else:
            yield self.positions

    def solve(self):
        for solution in self.getSolution(0):
            self.notify()


class Queen():
    def __init__(self):
        self.position = -1

    def update(self, pos):
        self.position = pos

    def getPosition(self):
        return self.position


class AnswerStorage():
    def __init__(self, queens):
        self.queens = queens
        self.answers = []

    def update(self):
        ans = []
        for q in self.queens:
            ans.append(q.getPosition())
        self.answers.append(ans)


if __name__ == "__main__":
    queens = []
    for i in range(8):
        queens.append(Queen())

    answer_storage = AnswerStorage(queens)
    solver = Solver(answer_storage)
    for q in queens:
        solver.addQueen(q)
    solver.solve()

    print(len(answer_storage.answers))
    for ans in answer_storage.answers:
        print(ans)
