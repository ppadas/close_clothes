"""Segmentator microservice"""

import os
from flask import Flask, request, Response, redirect
from flasgger import Swagger
from werkzeug.utils import secure_filename
from json import dumps
from common.markup_img import MarkupImg, ClotheType

app = Flask(__name__)
swagger = Swagger(app)

app.config["UPLOAD_FOLDER"] = "./upload"
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])


@app.route('/segment', methods=["POST"])
def segment():
    """Method for finding similar clothes by MarkupImg json
    ---
    parameters:
        - name: img
          in: formData
          required: true
          description: Image with desired clothes on it
          type: file
    responses:
        200:
          description: serialized MarkupImg object
          examples:
            cl_type: "dress"
    """
    print(request)
    print(request.files)
    if "img" not in request.files:
        return Response(response="No file was send", status=400)
    else:
        file = request.files["img"]
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))

        markup_img = MarkupImg(cl_type=ClotheType.DRESS)
        return Response(response=dumps(markup_img.to_dict()), status=200)

@app.route('/')
def index():
    """Default page"""
    return redirect('/apidocs')

app.run(debug=True, port=5002)
