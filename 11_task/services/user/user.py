"""User microservice"""

from json import loads
from flask import Flask, redirect, request
from flasgger import Swagger

app = Flask(__name__)
swagger = Swagger(app)

@app.route('/add_user_history', methods=['POST'])
def add_user_history():
    """Method for adding Search Result to users's history
    ---
    parameters:
        - name: search_result_json
          in: body
          required: true
          description: A JSON serialized instance of SearchResult
          type: string
        - name: user_id
          in: query
          required: true
          description: User id
          type: integer
          default: 1
    responses:
        200:
          description: ok
    """
    loads(request.data.decode("ascii"))
    return "User's history was updated."

@app.route('/')
def index():
    """Index page."""
    return redirect('/apidocs')

app.run(debug=True, port=5002)
