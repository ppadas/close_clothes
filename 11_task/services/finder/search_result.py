"""Search results of finder."""
from common.clothing_item import ClothingItem


class SearchResult:
    """Search Result class."""

    def __init__(self, items: list[ClothingItem]) -> None:
        self.__items = items

    def sort_by_price(self, reverse=False):
        """Sort Clothing Items by price"""
        self.__items.sort(key=lambda x: x.get_price(), reverse=reverse)

    def to_list(self) -> str:
        """Convert to python list."""
        res = []

        for item in self.__items:
            res.append(item.to_dict())

        return res
