"""Module for performing operations with database."""
import psycopg2
from common.brand import Brand
from common.clothing_item import ClothingItem


class Database:
    """Class for working with database."""

    def __init__(self) -> None:
        self.__conn = psycopg2.connect(
            dbname="close_clothes", user="olya", host="0.0.0.0", port="5432", password="nadya")
        self.__cur = self.__conn.cursor()

    def select_all(self):
        """Select all data from clothes table."""
        self.__cur.execute("SELECT * FROM clothes;")
        return self.__cur.fetchall()

    def select_category(self, cl_type: str):
        """Select all clothes of given category."""
        self.__cur.execute(
            f"SELECT category, description, brand, price, url FROM clothes WHERE category='{cl_type}';")
        return self.map_entity_to_dto(self.__cur.fetchall())

    def map_entity_to_dto(self, ent: list) -> list[ClothingItem]:
        """Maps raw objects from database to DTOs."""
        res = []
        for entity in ent:
            item = ClothingItem(cl_type=entity[0],
                                desc=entity[1],
                                brand=entity[2],
                                price=float(entity[3]),
                                url=entity[4])
            res.append(item)

        return res
