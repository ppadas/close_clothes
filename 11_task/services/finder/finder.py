"""Finder microservice"""

import requests
from flask import Flask, Response, redirect, request
from flasgger import Swagger
from database import Database

from json import loads, dumps
from common.markup_img import *
from common.clothing_item import ClothingItem
from search_result import SearchResult

app = Flask(__name__)
swagger = Swagger(app)


def add_result_to_history(user_id: int, res: SearchResult):
    """Add SearchResult to user's history."""
    requests.post(f'http://localhost:5002/add_user_history?user_id={user_id}', data=res, timeout=10)


@app.route('/search', methods=['POST'])
def search():
    """Method for finding similar clothes by MarkupImg json
    ---
    parameters:
        - name: user_id
          in: query
          required: true
          description: ID of user who initialized the search
          type: integer
          example: 1
        - name: markup_json
          in: body
          required: true
          description: A JSON serialized instance of MarkupImg
          type: string
          example: '{\"clothing_type\": \"dress\"}'
    responses:
        200:
          description: search results
    """
    markup_json = request.data.decode('ascii')
    user_id = request.args.get('user_id')

    if MarkupImg.is_valid_json(markup_json):
        database = Database()
        cl_type = MarkupImg.get_type_from_json(markup_json)
        items = database.select_category(cl_type)

        res = SearchResult(items)

        add_result_to_history(user_id, dumps(res.to_list(), indent=4, separators=(',', ':')))

        return dumps(res.to_list(), indent=4, separators=(',', ':'))

    else:
        return Response(status=400, response="Invalid json")


@app.route('/modify', methods=['POST'])
def modify():
    """Method for filtering SearchResult
    ---
    parameters:
        - name: search_result_json
          in: body
          required: true
          description: A JSON serialized instance of SearchResult
          type: string
        - name: price_up
          in: query
          required: true
          description: should prices be in ascending order or descending
          type: boolean
          default: true
    responses:
        200:
          description: modified version of Search Result
    """
    price_up = True if request.args.get('price_up') == "true" else False
    results = loads(request.data.decode("ascii"))
    items = []

    for result in results:
        item = ClothingItem(cl_type=result["type"],
                            desc=result["description"],
                            brand=result["brand"],
                            price=float(result["price"]),
                            url=result["url"])
        items.append(item)

    sr = SearchResult(items)
    sr.sort_by_price(not price_up)

    return dumps(sr.to_list(), indent=4, separators=(',', ':'))


@app.route('/')
def index():
    """Index page."""
    return redirect("/apidocs")


app.run(debug=True, port=5001)
