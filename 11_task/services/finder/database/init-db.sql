-- create
CREATE TABLE IF NOT EXISTS CLOTHES (
  itemId INTEGER PRIMARY KEY,
  category TEXT NOT NULL,
  description TEXT NOT NULL,
  brand TEXT NOT NULL,
  price NUMERIC(10,2) NOT NULL,
  url TEXT NOT NULL
);

-- insert
INSERT INTO CLOTHES VALUES (1, 'dress', 'description', 'Gloria Jeans', 1699, 'https://www.gloria-jeans.ru/p/GSO000385-1/Serebristoe-plate-s-vyrezom-halter');
INSERT INTO CLOTHES VALUES (2, 'dress', 'description', 'GUCCI', 206500, 'https://www.tsum.ru/product/5580207-plate-iz-shersti-i-khlopka-gucci-sinii/');
INSERT INTO CLOTHES VALUES (3, 'dress', 'description', 'OSTIN', 1599, 'https://ostin.com/product/printovannoe-plate-iz-shifona/25906950299?category-id=12485750299&availability=disabled');
INSERT INTO CLOTHES VALUES (4, 'jeans', 'description', 'Gloria Jeans', 2999, 'https://www.gloria-jeans.ru/p/GJN026134-1/Sirokie-dzinsy-Wide-leg-s-vysokoj-taliej');
INSERT INTO CLOTHES VALUES (5, 'jeans', 'description', 'GUCCI', 66900, 'https://www.tsum.ru/product/5580235-dzhinsy-gucci-sinii/');
INSERT INTO CLOTHES VALUES (6, 'jeans', 'description', 'Tom Tailor', 6599, 'https://tom-tailor.ru/catalog/men/muzhskie_dzhinsy/dzhinsy_josh_regular_slim_o18122428902/');
INSERT INTO CLOTHES VALUES (7, 'jeans', 'description', 'OSTIN', 1599, 'https://ostin.com/product/bazovye-superuzkie-dzhinsy-s-vysokoj-posadkoj/23557640299?category-id=15484250299&availability=disabled');
INSERT INTO CLOTHES VALUES (8, 'shirt', 'description', 'Tom Tailor', 5599, 'https://tom-tailor.ru/catalog/men/muzhskie_jemperi_i_sviteri/pulover_103230421652/');
INSERT INTO CLOTHES VALUES (9, 'shirt', 'description', 'OSTIN', 1199, 'https://ostin.com/product/bluzka-iz-satina/26856980299?category-id=12485650299&availability=disabled');
INSERT INTO CLOTHES VALUES (10, 't-shirt', 'description', 'OSTIN', 1599, 'https://ostin.com/product/futbolka-s-pajetkami/28035820299?category-id=12485690299&availability=disabled');
INSERT INTO CLOTHES VALUES (11, 't-shirt', 'description', 'Tom Tailor', 3299, 'https://tom-tailor.ru/catalog/women/zhenskie_futbolki_i_topi/futbolka_103016828946/');
INSERT INTO CLOTHES VALUES (12, 't-shirt', 'description', 'TEZENIS', 1299, 'https://www.tezenis.com/ru/product/%D0%BA%D0%BE%D1%80%D0%BE%D1%82%D0%BA%D0%B0%D1%8F_%D1%85%D0%BB%D0%BE%D0%BF%D0%BA%D0%BE%D0%B2%D0%B0%D1%8F_%D1%84%D1%83%D1%82%D0%B1%D0%BE%D0%BB%D0%BA%D0%B0_%D1%81_%D0%BF%D0%BE%D1%8F%D1%81%D0%BE%D0%BC_%D0%B2_%D1%80%D1%83%D0%B1%D1%87%D0%B8%D0%BA-1MM1029.html?dwvar_1MM1029_Z_COL_TEZD=310U');
INSERT INTO CLOTHES VALUES (13, 'trousers', 'description', 'Gloria Jeans', 2499, 'https://www.gloria-jeans.ru/p/BAC009808-2/Koricnevye-sportivnye-bruki-Comfort-s-printom');
INSERT INTO CLOTHES VALUES (14, 'trousers', 'description', 'GUCCI', 62950, 'https://www.tsum.ru/product/5558687-bryuki-iz-viskozy-gucci-rozovyi/');
INSERT INTO CLOTHES VALUES (15, 'trousers', 'description', 'Tom Tailor', 6599, 'https://tom-tailor.ru/catalog/men/muzhskie_bryuki/briuki_o19012350708/');
INSERT INTO CLOTHES VALUES (16, 'trousers', 'description', 'Tom Tailor', 2999, 'https://tom-tailor.ru/catalog/women/zhenskie_briuki/bryuki_103199028995/');
INSERT INTO CLOTHES VALUES (17, 'trousers', 'description', 'OSTIN', 3599, 'https://ostin.com/product/kozhanye-pryamye-bryuki/27583980299?category-id=12485630299&availability=disabled');
INSERT INTO CLOTHES VALUES (18, 'skirt', 'description', 'Tom Tailor', 4599, 'https://tom-tailor.ru/catalog/women/yubki/yubka_103359214482/');
INSERT INTO CLOTHES VALUES (19, 'skirt', 'description', 'OSTIN', 1199, 'https://ostin.com/product/yubka-karandash-s-pugovitsami/25906670299?category-id=12485760299&availability=disabled');
INSERT INTO CLOTHES VALUES (20, 'skirt', 'description', 'TEZENIS', 2999, 'https://www.tezenis.com/ru/product/%D0%B4%D0%BB%D0%B8%D0%BD%D0%BD%D0%B0%D1%8F_%D1%8E%D0%B1%D0%BA%D0%B0_%D0%B8%D0%B7_%D1%82%D0%BA%D0%B0%D0%BD%D0%B8_%D1%81_%D0%BE%D0%B1%D0%BE%D1%80%D0%BA%D0%BE%D0%B9-1WG1456.html?dwvar_1WG1456_Z_COL_TEZD=840U');
INSERT INTO CLOTHES VALUES (21, 'shoes', 'description', 'GUCCI', 151500, 'https://www.tsum.ru/product/5603538-kombinirovannye-sapogi-finn-gucci-bordovyi/');
INSERT INTO CLOTHES VALUES (22, 'shoes', 'description', 'Tom Tailor', 3499, 'https://tom-tailor.ru/catalog/women/zhenskie_krossovki/obuv_11906040070O764/');
INSERT INTO CLOTHES VALUES (23, 'shoes', 'description', 'Jordan', 10344, 'https://www.wildberries.ru/catalog/136963751/detail.aspx?targetUrl=BP');
INSERT INTO CLOTHES VALUES (24, 'hat', 'description', 'Tom Tailor', 1999, 'https://tom-tailor.ru/catalog/men/muzhskie_golovnye_ubory/kepka_103072227505/');
INSERT INTO CLOTHES VALUES (25, 'hat', 'description', 'GUCCI', 55500, 'https://www.tsum.ru/product/5580211-khlopkovyi-beret-gucci-krasnyi/');