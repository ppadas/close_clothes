"""Module containing MarkupImg implementation."""

from enum import Enum
from json import loads


class ClotheType(Enum):
    """Clothe types for MarkupImg."""
    DRESS = 1
    T_SHIRT = 2
    POLO = 3
    SKIRT = 4
    # Enough for demo


class MarkupImg:
    """This class is used for selecting types of clothes found on image."""

    def __init__(self, cl_type: ClotheType) -> None:
        self.__cl_type = cl_type

    @property
    def clothe_type(self):
        """Getter"""
        return self.__cl_type

    def __str__(self) -> str:
        return str({"clothing_type": self.__cl_type.name.lower()})

    def to_dict(self) -> dict:
        """Convert to python dictionary."""
        return {"clothing_type": self.__cl_type.name.lower()}

    @staticmethod
    def is_valid_json(s_json: str) -> bool:
        """Check if json configuration is correct."""
        try:
            o_json = loads(s_json)

            return len(o_json) == 1 and "clothing_type" in o_json
        except:
            return False

    @staticmethod
    def get_type_from_json(s_json: str) -> str:
        """Get clothing type from json description."""
        o_json = loads(s_json)

        return o_json["clothing_type"]
