"""Brand module."""


class Brand:
    """Brand."""

    def __init__(self, info: str, name: str, addresses: list[str]) -> None:
        self.__info = info
        self.__name = name
        self.__addresses = addresses

    def get_info(self) -> str:
        """Get brand's info."""
        return self.__info

    def get_name(self) -> str:
        """Get brand's name."""
        return self.__name

    def get_addresses(self) -> list[str]:
        """Get brand's addresses."""
        return self.__addresses
