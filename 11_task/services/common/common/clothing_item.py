"""Clothing item module."""
from common.brand import Brand


class ClothingItem:
    """Clothing item."""

    def __init__(self, desc: str, url: str, brand: Brand, price: int, cl_type: str) -> None:
        self.__desc = desc
        self.__url = url
        self.__brand = brand
        self.__price = price
        self.__cl_type = cl_type

    def get_desc(self) -> str:
        """Get description of clothing item."""
        return self.__desc

    def get_url(self) -> str:
        """Get url for clothing item."""
        return self.__url

    def get_brand(self) -> Brand:
        """Get clothing item's brand."""
        return self.__brand

    def get_price(self) -> int:
        """Get clothing item's price."""
        return self.__price

    def get_type(self) -> str:
        """Get type of clothing item."""
        return self.__cl_type

    def __str__(self) -> str:
        return str(self.to_dict())

    def to_dict(self) -> dict:
        """Convert to python dictionary."""
        d = {}
        d["description"] = self.__desc
        d["url"] = self.__url
        d["brand"] = self.__brand
        d["price"] = self.__price
        d["type"] = self.__cl_type

        return d